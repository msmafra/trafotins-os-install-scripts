#!/usr/bin/env bash
LC_ALL=C
LANG=C

declare GITHUB_UC_URL
declare GITLAB_URL
declare ETC_SYSTEMD

GITHUB_UC_URL="https://raw.githubusercontent.com"
GITLAB_URL="https://gitlab.com"
ETC_SYSTEMD="/etc/systemd/system"

main() {
    # Checks if is root to not use sudo or doas
    if [[ "${USER}" != root ]]; then
        # checks if doas is installed and use it as default
        [[ -f /usr/bin/doas ]] && privup="doas" || privup="sudo"
    fi
    # Checks if dnf5 is available!!!
    [[ -f /usr/bin/dnf5 ]] && dnfver="dnf5" || dnfver="dnf"

    trafotin_enable_repos
    trafotin_debloat
    trafotin_dnf_cnf
    trafotin_firmware_update
    trafotin_umask
    # trafotin_oomd_check
    trafotin_gnome_stuff
    trafotin_installations
    trafotin_swaying
    trafotin_kicksecure
    # trafotin_ssh_limits
    trafoting_nts
    trafotin_firewall_stuff
    trafotin_mac_rand
    trafotin_services
    # trafotin_systemd_resolved
    trafotin_secure_home
    # trafotin_davinci

    message progress "The configuration is now complete."
}
#######################################
# Prints coloured messages for specific purposes
# Arguments:
#   message_type, msg
# Outputs:
#   Colored string message based on message_type input
# this was inspired by Wimpy's World (Martin Wimpress) deb-get fancy_message function
#######################################
message() {
    # https://robotmoon.com/256-colors/
    # https://www.color-hex.com/color-names.html

    [ -z "${1}" ] || [ -z "${2}" ] && return

    local count_colours
    local message_type
    local msg
    local -i red
    local -i blue
    local -i green
    local -i white
    local -i grey
    local -i lightblue
    local -i purple
    local -i scarlet
    local -i orange
    local -i cyan

    count_colours="$(tput colors)"
    message_type="${1}"
    shift
    msg="${1}"
    if [[ "${count_colours}" == 256 ]]; then
        red="124"
        blue="21"
        green="46"
        white="255"
        grey="241"
        lightblue="33"
        purple="93"
        scarlet="160"
        yellow="226"
        orange="208"
        cyan="39"
    else
        red="1"
        blue="4"
        green="2"
        white="15"
        grey="8"
        lightblue="12"
        purple="5"
        scarlet="9"
        yellow="11"
        orange="${yellow}"
        cyan="14"
    fi

    case ${message_type} in
    error) colourize "${red}" "[e] ${msg}" | \tee >&2 ;;
    fatal) colourize "${scarlet}" "[f] ${msg}" | \tee >&2 ;;
    info) colourize "${lightblue}" "[i] ${msg}" ;;
    bullets) colourize "${cyan}" "    -> ${msg}" ;;
    progress) colourize "${white}" "[p] ${msg}" ;;
    recommend) colourize "${cyan}" "[r] ${msg}" ;;
    warn) colourize "${orange}" "[w] ${msg}" ;;
    purple) colourize "${purple}" "${msg}" ;;
    grey) colourize "${grey}" "${msg}" ;;
    green) colourize "${green}" "${msg}" ;;
    blue) colourize "${blue}" "${msg}" ;;
    yellow) colourize "${yellow}" "${msg}" ;;
    *) printf "  - ? - %s - ? -\n" "${message_type}" ;;
    esac
}
#######################################
# Prints simple info log style for each function
# Globals: None
# Arguments: FUNCNAME
# Outputs: None
#######################################
log() {
    if [[ "${*}" = log ]]; then
        printf "(LOGGING) %s : *" "${FUNCNAME[1]:-unknown}"
    fi
}
trafotin_dnf_cnf() {

    log "trafotin_dnf_cnf"
    local dnf_cnf
    dnf_cnf="/etc/dnf/dnf.conf"

    message warning "Make sure your system has been fully-updated by running ${privup} ${dnfver} upgrade or ${privup} ${dnfver} distro-sync and reboot it once."
    ${privup} cp --verbose "${dnf_cnf}"{,.ORIGINAL}
    # Configure dnf (In order: automatically select fastest mirror, parallel downloads, and disable telemetry)
    # fastestmirror=1
    printf "%s" "
max_parallel_downloads=10
countme=false
" | ${privup} tee --append "${dnf_cnf}"

    # ${privup} "${dnfver}" --assumeyes distro-sync
    ${privup} "${dnfver}" --assumeyes upgrade

}
trafotin_umask() {

    log "trafotin_umask"
    #Setting umask to 077
    # No one except wheel user and root get read/write files
    umask 077
    ${privup} sed --in-place 's/umask 002/umask 077/g' /etc/bashrc
    ${privup} sed --in-place 's/umask 022/umask 077/g' /etc/bashrc
}
trafotin_debloat() {

    log "trafotin_debloat"
    local -a debloating_stuff
    debloating_stuff=(
        "abrt*"
        "adcli"
        "alsa-sof-firmware"
        "anaconda*"
        "anthy-unicode"
        "atmel-firmware"
        "avahi"
        "baobab"
        "bluez-cups"
        "boost-date-time"
        "brasero-libs"
        "cheese"
        "cyrus-sasl-plain"
        "dmidecode"
        # "dnf5"
        # "doas"
        "dos2unix"
        "eog"
        "evince-djvu"
        "evince"
        "fedora-bookmarks"
        "fedora-chromium-config"
        "firefox"
        "geolite2*"
        "gnome-backgrounds"
        "gnome-boxes"
        "gnome-calculator"
        "gnome-calendar"
        "gnome-characters"
        "gnome-classic-session"
        "gnome-clocks"
        "gnome-color-manager"
        "gnome-connections"
        "gnome-contacts"
        "gnome-font-viewer"
        "gnome-font-viewer"
        "gnome-logs"
        "gnome-maps"
        "gnome-remote-desktop"
        "gnome-shell-extension-background-logo"
        "gnome-shell-extension*"
        "gnome-system-monitor"
        "gnome-text-editor"
        "gnome-themes-extra"
        "gnome-tour"
        "gnome-tour"
        "gnome-user-docs"
        "gnome-weather"
        "hyperv*"
        "kpartx"
        "libertas-usb8388-firmware"
        "linux-firmware"
        "mailcap"
        "mediawriter"
        # "micro"
        "ModemManager"
        "mozilla-filesystem"
        "mtr"
        "nano-default-editor"
        "nano"
        "NetworkManager-ssh"
        "nmap-ncat"
        "open-vm-tools"
        "openconnect"
        "openvpn"
        "orca"
        "perl-IO-Socket-SSL"
        "perl*"
        "podman"
        "ppp"
        "pptp"
        "qgnomeplatform"
        "realmd"
        "rsync"
        "samba-client"
        "sane*"
        "simple-scan"
        "sos"
        "spice-vdagent"
        "sssd"
        "tcpdump"
        "teamd"
        "thermald"
        "totem"
        "traceroute"
        "trousers"
        "unbound-libs"
        "virtualbox-guest-additions"
        "vpnc"
        "xorg-x11-drv-vmware"
        "yajl"
        "yelp"
        "zd1211-firmware"
    )
    # Debloat
    ${privup} "${dnfver}" --assumeyes remove "${debloating_stuff[*]}"
}
trafotin_oomd_check() {
    log "trafotin_oomd_check"
    # Verify systemd-oomd works
    systemctl status systemd-oomd
}
trafotin_firmware_update() {
    # Run Updates
    ${privup} "${dnfver}" autoremove --assumeyes
    ${privup} fwupdmgr get-devices
    ${privup} fwupdmgr refresh --force
    ${privup} fwupdmgr get-updates --assume-yes
    ${privup} fwupdmgr update --assume-yes
}
trafotin_gnome_stuff() {
    log "trafotin_gnome_stuff"
    message info "Customizing Gnome"
    # Configure GNOME
    gsettings set org.gnome.desktop.a11y always-show-universal-access-status true
    #gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
    gsettings set org.gnome.desktop.interface clock-show-weekday true
    gsettings set org.gnome.desktop.interface clock-show-seconds true
    #gsettings set org.gnome.desktop.screensaver.lock-enabled false
    #gsettings set org.gnome.desktop.notifications.show-in-lock-screen false
    gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
}
trafotin_enable_repos() {

    log "trafotin_enable_repos"
    local rpmfusion_url
    rpmfusion_url="https://mirrors.rpmfusion.org/free/fedora"
    # Setup Flathub and Flatpak
    # Flathub is enabled by default, but fails to install anything outside of Fedora still.
    # Alternatively you can enable third party repos at install, but this clutters dnf with NVIDIA and Chrome.
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
    # Setup RPMFusion
    ${privup} "${dnfver}" install -y ${rpmfusion_url}/rpmfusion-free-release-"$(rpm -E %fedora)".noarch.rpm ${rpmfusion_url}/rpmfusion-nonfree-release-"$(rpm -E %fedora)".noarch.rpm
    ${privup} "${dnfver}" groupupdate core -y

}
trafotin_installations() {
    log "trafotin_installations"

    # Install things I need
    local -a dnf_installs
    local -a flatpak_installs
    local -i map_count_current
    local -i map_count_set
    dnf_installs=(
        "@virtualization"
        "akmod-v4l2loopback"
        "compat-ffmpeg4"
        "distrobox"
        "ffmpeg"
        "hugo"
        "neovim"
        "newsboat"
        "podman"
        "setroubleshoot"
        "sqlite3"
        "steam-devices"
        "yt-dlp"
        "zsh-autosuggestions"
        "zsh-syntax-highlighting"
    )
    flatpak_installs=(
        "app.drey.Warp"
        "ch.protonmail.protonmail-bridge"
        "com.brave.Browser"
        "com.github.micahflee.torbrowser-launcher"
        "com.github.tchx84.Flatseal"
        "com.heroicgameslauncher.hgl"
        "com.obsproject.Studio.Plugin.OBSVkCapture"
        "com.obsproject.Studio"
        "com.protonvpn.www"
        "com.transmissionbt.Transmission"
        "com.tutanota.Tutanota"
        "com.usebottles.bottles"
        "com.valvesoftware.Steam"
        "io.freetubeapp.FreeTube"
        "net.davidotek.pupgui2"
        "net.mullvad.MullvadBrowser"
        "org.bleachbit.BleachBit"
        "org.blender.Blender"
        "org.freedesktop.Platform.VulkanLayer.MangoHud"
        "org.freedesktop.Platform.VulkanLayer.OBSVkCapture"
        "org.getmonero.Monero"
        "org.getmonero.Monero"
        "org.gnome.Calculator"
        "org.gnome.Characters"
        "org.gnome.Evince"
        "org.gnome.Evolution"
        "org.gnome.Extensions"
        "org.gnome.gitlab.YaLTeR.VideoTrimmer"
        "org.gnome.Loupe"
        "org.inkscape.Inkscape"
        "org.keepassxc.KeePassXC"
        "org.libreoffice.LibreOffice"
        "org.mozilla.firefox"
        "org.pipewire.Helvum"
        "org.signal.Signal"
        "re.sonny.Junction"
    )
    map_count_current="$(sysctl --values vm.max_map_count)"
    map_count_set="2147483642"

    flatpak install --assumeyes flathub "${flatpak_installs[*]}"
    ${privup} "${dnfver}" --assumeyes --best --allowerasing install "${dnf_installs[*]}"

    message progress "Use flatpak MangoHUD"
    flatpak override --user --filesystem=xdg-config/MangoHud:ro com.valvesoftware.Steam

    # https://fedoraproject.org/wiki/Changes/IncreaseVmMaxMapCount#Detailed_Description
    message progress "Sets vm.max_map_count from Fedora's default ${map_count_current} to ${map_count_set}"
    message info "${privup} sysctl -w vm.max_map_count=${map_count_set} can change temporarelly for testing"

    ${privup} tee --ignore-interrupts /etc/sysctl.d/99-sysctl.conf <<MAXMAPCOUNT
# sysctl settings are defined through files in
# /usr/lib/sysctl.d/, /run/sysctl.d/, and /etc/sysctl.d/.
#
# Vendors settings live in /usr/lib/sysctl.d/.
# To override a whole file, create a new file with the same in
# /etc/sysctl.d/ and put new settings there. To override
# only specific settings, add a file with a lexically later
# name in /etc/sysctl.d/ and put new settings there.
#
# For more information, see sysctl.conf(5) and sysctl.d(5).
vm.max_map_count=${map_count_set}
MAXMAPCOUNT
    message info "After rebooting, run flatpak update to have NVIDIA drivers install for flatpak apps (Steam and others)"

}
trafotin_swaying() {

    log "trafotin_swaying"

    local -a sway_pkgs
    sway_pkgs=(
        "sway"
        "waybar"
        "wlsunset"
        "network-manager-apple"
    )

    message info "Install Sway"
    ${privup} "${dnfver}" install -y "${sway_pkgs[*]}"
}
trafotin_kicksecure() {

    log "trafotin_kicksecure"

    local kicksecure_path
    kicksecure_path="Kicksecure/security-misc/master/etc"

    # Harden the Kernel with Kicksecure's patches
    ${privup} curl "${GITHUB_UC_URL}"/"${kicksecure_path}"/modprobe.d/30_security-misc.conf --output /etc/modprobe.d/30_security-misc.conf
    ${privup} curl "${GITHUB_UC_URL}"/"${kicksecure_path}"/sysctl.d/30_security-misc.conf --output /etc/sysctl.d/30_security-misc.conf
    ${privup} curl "${GITHUB_UC_URL}"/"${kicksecure_path}"/sysctl.d/30_silent-kernel-printk.conf --output /etc/sysctl.d/30_silent-kernel-printk.conf

    # Enable Kicksecure CPU mitigations
    ${privup} curl "${GITHUB_UC_URL}"/"${kicksecure_path}"/default/grub.d/40_cpu_mitigations.cfg --output /etc/grub.d/40_cpu_mitigations.cfg
    # Kicksecure's CPU distrust script
    ${privup} curl "${GITHUB_UC_URL}"/"${kicksecure_path}"/default/grub.d/40_distrust_cpu.cfg --output /etc/grub.d/40_distrust_cpu.cfg
    # Enable Kicksecure's IOMMU patch (limits DMA)
    ${privup} curl "${GITHUB_UC_URL}"/"${kicksecure_path}"/default/grub.d/40_enable_iommu.cfg --output /etc/grub.d/40_enable_iommu.cfg

    # Divested's brace patches
    # Sandbox the brace systemd permissions
    message info "If you have VPN issues: https://old.reddit.com/r/DivestOS/comments/12b4fk4/comment/jex4qt2/"
    ${privup} mkdir --parent --verbose "${ETC_SYSTEMD}"/NetworkManager.service.d
    ${privup} curl "${GITLAB_URL}"/divested/brace/-/raw/master/brace/usr/lib/systemd/system/NetworkManager.service.d/99-brace.conf --output "${ETC_SYSTEMD}"/NetworkManager.service.d/99-brace.conf
    ${privup} mkdir --parent --verbose "${ETC_SYSTEMD}"/irqbalance.service.d
    ${privup} curl "${GITLAB_URL}"/divested/brace/-/raw/master/brace/usr/lib/systemd/system/irqbalance.service.d/99-brace.conf --output "${ETC_SYSTEMD}"/irqbalance.service.d/99-brace.conf
}
trafotin_ssh_limits() {
    log "trafotin_ssh_limits"
    # GrapheneOS's ssh limits
    # caps the system usage of sshd
    # GrapheneOS has changed the way this is implemented, so I'm working on a reintegration.
    ${privup} mkdir --parent --verbose "${ETC_SYSTEMD}"/sshd.service.d
    ${privup} curl "${GITHUB_UC_URL}"/GrapheneOS/infrastructure/main/systemd/system/sshd.service.d/limits.conf --output "${ETC_SYSTEMD}"/sshd.service.d/limits.conf
    printf "%s" "GSSAPIAuthentication no" | ${privup} tee --append /etc/ssh/ssh_config.d/10-custom.conf
    printf "%s" "VerifyHostKeyDNS yes" | ${privup} tee --append /etc/ssh/ssh_config.d/10-custom.conf
}
trafoting_nts() {
    log "trafoting_nts"
    # NTS instead of NTP
    # NTS is a more secured version of NTP
    # During installtion, Anaconda let you choose this, if you didn't this will make the change for you
    ${privup} curl "${GITHUB_UC_URL}"/GrapheneOS/infrastructure/main/chrony.conf --output /etc/chrony.conf
}
trafotin_firewall_stuff() {
    log "trafotin_firewall_stuff"
    # Remove Firewalld's Default Rules
    ${privup} firewall-cmd --permanent --remove-port=1025-65535/udp
    ${privup} firewall-cmd --permanent --remove-port=1025-65535/tcp
    ${privup} firewall-cmd --permanent --remove-service=mdns
    ${privup} firewall-cmd --permanent --remove-service=ssh
    ${privup} firewall-cmd --permanent --remove-service=samba-client
    ${privup} firewall-cmd --reload
}
trafotin_mac_rand() {
    log "trafotin_mac_rand"

    message info "Randomize MAC address and disable static hostname. This could be used to track general network activity."

    ${privup} tee --ignore-interrupts "/etc/NetworkManager/conf.d/00-macrandomize.conf" <<RANDOMMAC
[main]
hostname-mode=none

[device]
wifi.scan-rand-mac-address=yes

[connection]
wifi.cloned-mac-address=random
ethernet.cloned-mac-address=random
RANDOMMAC

    ${privup} systemctl restart NetworkManager
    ${privup} hostnamectl hostname "localhost"
}
trafotin_services() {
    log "trafotin_services"

    # Cockpit is still missing some core functionality, but will switch when it is added.
    # ${privup} systemctl enable cockpit.socket --now
    # Disable Bluetooth
    ${privup} systemctl disable --now bluetooth
    # Initialize virtualization
    ${privup} sed --in-place 's/#unix_sock_group = "libvirt"/unix_sock_group = "libvirt"/g' /etc/libvirt/libvirtd.conf
    ${privup} sed --in-place 's/#unix_sock_rw_perms = "0770"/unix_sock_rw_perms = "0770"/g' /etc/libvirt/libvirtd.conf
    ${privup} systemctl enable libvirtd

}
trafotin_systemd_resolved() {
    log "trafotin_systemd_resolved"

    local nm_resolved_file
    # local resolved_conf

    nm_resolved_file="/etc/NetworkManager/conf.d/10-dns-systemd-resolved.conf"
    # resolved_conf="/etc/systemd/resolved.conf"

    ${privup} cp --verbose "${nm_resolved_file}"{,.ORIGINAL}
    ${privup} cp --verbose "${nm_resolved_file}"{,.ORIGINAL}
    ${privup} sed --in-file 's/#dns=systemd-resolved/dns=systemd-resolved/g'
    ${privup} sed --in-file 's/#systemd-resolved=false/systemd-resolved=false/g'
    # tee --ignore-interrupsts /etc/systemd/resolved.conf <<RESOLVED
    # [Resolve]
    # # https://developers.cloudflare.com/1.1.1.1/1.1.1.1-for-families
    # # https://support.quad9.net/hc/en-us/articles/360041193212-Quad9-IPs-and-other-settings
    # # https://en.wikipedia.org/wiki/Public_recursive_name_server
    # # Using Quad9, CloudFlare and AdGuard
    # DNS=9.9.9.9 149.112.112.112 2620:fe::fe 2620:fe::9
    # DNS=1.1.1.1 1.0.0.1 2606:4700:4700::1111 2606:4700:4700::1001
    # DNS=94.140.14.14 94.140.15.15 2a10:50c0::ad1:ff 2a10:50c0::ad2:ff
    # FallbackDNS=149.112.112.11 9.9.9.11 2620:fe::11 2620:fe::fe:11
    # FallbackDNS=1.1.1.2 1.0.0.2 1.1.1.3 1.0.0.3 2606:4700:4700::1112 2606:4700:4700::1002 2606:4700:4700::1113 2606:4700:4700::1003
    # FallbackDNS=94.140.14.15 94.140.15.16 2a10:50c0::bad1:ff 2a10:50c0::bad2:ff
    # Domains=~.
    # DNSSEC=yes
    # #DNSSEC=no
    # DNSOverTLS=yes
    # LLMNR=no
    # Cache=yes
    # Enable DNSSEC
    # causes severe network instability, but working on getting this up and running
    # ${privup} sed --in-file 's/#DNSSEC=no/DNSSEC=yes/g' "${resolved_conf}"
    # ${privup} systemctl restart systemd-resolved
    # RESOLVED

    message info "Testing address without DNSSEC enabled (must error out)"
    resolvectl query badsig.go.dnscheck.tools

    message info "Testing DNSSEC enabled address (must not error out)"
    resolvectl query go.dnscheck.tools

}
trafotin_secure_home() {
    log "trafotin_secure_home"
    # Make the Home folder private
    # Privatizing the home folder creates problems with virt-manager
    # accessing ISOs from your home directory. Store images in /var/lib/libvirt/images
    #chmod --verbose 700 /home/"$(whoami)"
    chmod --verbose 700 "${HOME}"
    # is reset using:
    #chmod 755 /home/"$(whoami)"
}
trafotin_davinci() {
    log "trafotin_davinci"
    # DaVinci Resolve tweaks
    # Because no one ever said how to in detail
    # I'm sorry GE, but this might as well be nonsense to normies: https://old.reddit.com/r/Fedora/comments/12g0mh4/fedora_38_issue_with_davinci_resolve/
    # ${privup} "${dnfver}" install mesa-Glu
    #${privup} cp /lib64/libglib-2.0.so.0* /opt/resolve/libs
}
# Run it
main "${@}"
