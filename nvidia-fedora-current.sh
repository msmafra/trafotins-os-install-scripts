#!/usr/bin/env bash
LC_ALL=C
LANG=C
#
# Installs NVIDIA driver
# Requires RPMFusion
# Sauce: https://rpmfusion.org/Howto/NVIDIA
# I have a 1080TI, so I install NVIDIA, the Xorg CUDA libraries for CUDA acceleration and NVENC codecs, and vdpau for accerlated video things.
# You can also install the 390x version, which I don't have any compatible devices for, but I hear nouveau is probably the way to go, because of the drivers are EOL.
trafotin_nvidia_install() {

    local privup
    local -a nvidia_drivers
    # local script_folder
    # script_folder="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
    nvidia_drivers=(
        "kmod-nvidia"
        "xorg-x11-drv-nvidia-cuda"
        "akmod-nvidia"
        "nvidia-vaapi-driver"
        "libva-utils"
        "vdpauinf"
    )
    # Checks if is root to not use sudo or doas
    if [[ "${USER}" != root ]]; then
        # checks if doas is installed and use it as default
        [[ -f /usr/bin/doas ]] && privup="doas" || privup="sudo"
    fi
    # Checks if dnf5 is available!!!
    [[ -f /usr/bin/dnf5 ]] && dnfver="dnf5" || dnfver="dnf"
    ${privup} "${dnfver}" install "${nvidia_drivers[*]}"

    # I'm not using RPMFusion, but with NEGATIVO17's repos the addition of nvidia-drm.modeset=1 to GRUB isn't necessary
    # it is added to the /etc/modprobe.d/nvidia-modeset.conf file:
    # Nvidia modesetting support. There is NO console graphical framebuffer (like
    # OSS drivers); this is only for Wayland support.
    #
    # Set to 0 or comment to disable kernel modesetting support. This must be
    # disabled in case of Mosaic or SLI.
    #options nvidia-drm modeset=1

    message progress "Adding nvidia-drm.modeset to /etc/default/grub. GRUB_CMDLINE_LINUX line"
    ${privup} grubby --update-kernel=ALL --args='nvidia-drm.modeset=1'
    message progress "Updating grub config with ${privup} grub2-mkconfig -o /boot/grub2/grub.cfg command"
    ${privup} grub2-mkconfig -o /boot/grub2/grub.cfg

}
#######################################
# Prints coloured messages for specific purposes
# Arguments:
#   message_type, msg
# Outputs:
#   Colored string message based on message_type input
# this was inspired by Wimpy's World (Martin Wimpress) deb-get fancy_message function
#######################################
message() {
    # https://robotmoon.com/256-colors/
    # https://www.color-hex.com/color-names.html

    [ -z "${1}" ] || [ -z "${2}" ] && return

    local count_colours
    local message_type
    local msg
    local -i red
    local -i blue
    local -i green
    local -i white
    local -i grey
    local -i lightblue
    local -i purple
    local -i scarlet
    local -i orange
    local -i cyan

    count_colours="$(tput colors)"
    message_type="${1}"
    shift
    msg="${1}"
    if [[ "${count_colours}" == 256 ]]; then
        red="124"
        blue="21"
        green="46"
        white="255"
        grey="241"
        lightblue="33"
        purple="93"
        scarlet="160"
        yellow="226"
        orange="208"
        cyan="39"
    else
        red="1"
        blue="4"
        green="2"
        white="15"
        grey="8"
        lightblue="12"
        purple="5"
        scarlet="9"
        yellow="11"
        orange="${yellow}"
        cyan="14"
    fi

    case ${message_type} in
    error) colourize "${red}" "[e] ${msg}" | \tee >&2 ;;
    fatal) colourize "${scarlet}" "[f] ${msg}" | \tee >&2 ;;
    info) colourize "${lightblue}" "[i] ${msg}" ;;
    bullets) colourize "${cyan}" "    -> ${msg}" ;;
    progress) colourize "${white}" "[p] ${msg}" ;;
    recommend) colourize "${cyan}" "[r] ${msg}" ;;
    warn) colourize "${orange}" "[w] ${msg}" ;;
    purple) colourize "${purple}" "${msg}" ;;
    grey) colourize "${grey}" "${msg}" ;;
    green) colourize "${green}" "${msg}" ;;
    blue) colourize "${blue}" "${msg}" ;;
    yellow) colourize "${yellow}" "${msg}" ;;
    *) printf "  - ? - %s - ? -\n" "${message_type}" ;;
    esac
}
trafotin_nvidia_install
