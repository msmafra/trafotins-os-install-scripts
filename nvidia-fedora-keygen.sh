#!/usr/bin/env bash
LC_ALL=C
LANG=C
#
# Installs and automatically signs the NVIDIA driver for UEFI secure boot
# Sauce: https://rpmfusion.org/Howto/Secure%20Boot
# This also causes a "false flag" with GNOME/KDE's security menu, although it's not wrong.
# Your system must be installed with UEFI enabled out of the box, which if you have Secure
# Boot enabled prior to the installation, it works great.
#
# Runs everything
main() {
    # Checks if is root to not use sudo or doas
    if [[ "${USER}" != root ]]; then
        # checks if doas is installed and use it as default
        [[ -f /usr/bin/doas ]] && privup="doas" || privup="sudo"
    fi
    # Checks if dnf5 is available!!!
    [[ -f /usr/bin/dnf5 ]] && dnfver="dnf5" || dnfver="dnf"
    message progress "Installing needed tools"
    ${privup} "${dnfver}" --assumeyes install kmodtool akmods openssl
    message progress "Generates default values for the cacert.config file"
    ${privup} kmodgenca --auto
    message progress "Importing the key"
    ${privup} mokutil --import /etc/pki/akmods/certs/public_key.der
    message info "Reboot, enter your key's password, then run script 2."
}
#######################################
# Prints coloured messages for specific purposes
# Arguments:
#   message_type, msg
# Outputs:
#   Colored string message based on message_type input
# this was inspired by Wimpy's World (Martin Wimpress) deb-get fancy_message function
#######################################
message() {
    # https://robotmoon.com/256-colors/
    # https://www.color-hex.com/color-names.html

    [ -z "${1}" ] || [ -z "${2}" ] && return

    local count_colours
    local message_type
    local msg
    local -i red
    local -i blue
    local -i green
    local -i white
    local -i grey
    local -i lightblue
    local -i purple
    local -i scarlet
    local -i orange
    local -i cyan

    count_colours="$(tput colors)"
    message_type="${1}"
    shift
    msg="${1}"
    if [[ "${count_colours}" == 256 ]]; then
        red="124"
        blue="21"
        green="46"
        white="255"
        grey="241"
        lightblue="33"
        purple="93"
        scarlet="160"
        yellow="226"
        orange="208"
        cyan="39"
    else
        red="1"
        blue="4"
        green="2"
        white="15"
        grey="8"
        lightblue="12"
        purple="5"
        scarlet="9"
        yellow="11"
        orange="${yellow}"
        cyan="14"
    fi

    case ${message_type} in
    error) colourize "${red}" "[e] ${msg}" | \tee >&2 ;;
    fatal) colourize "${scarlet}" "[f] ${msg}" | \tee >&2 ;;
    info) colourize "${lightblue}" "[i] ${msg}" ;;
    bullets) colourize "${cyan}" "    -> ${msg}" ;;
    progress) colourize "${white}" "[p] ${msg}" ;;
    recommend) colourize "${cyan}" "[r] ${msg}" ;;
    warn) colourize "${orange}" "[w] ${msg}" ;;
    purple) colourize "${purple}" "${msg}" ;;
    grey) colourize "${grey}" "${msg}" ;;
    green) colourize "${green}" "${msg}" ;;
    blue) colourize "${blue}" "${msg}" ;;
    yellow) colourize "${yellow}" "${msg}" ;;
    *) printf "  - ? - %s - ? -\n" "${message_type}" ;;
    esac
}
main "${@}"
